package com.fitbit.client;

import com.fitbit.model.User;
import com.fitbit.model.VersionedEntity;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.GenericType;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: Aliaksandr Barysiuk (abarysiuk@fitbit.com)
 * @date: 27/05/2014
 */
public class FirstClient extends GenericClient {

    public static final String V1_0 = "application/v1.0+json";
    public static final String V1_1 = "application/v1.1+json";

    private static final Map<String, Double> VERSIONS = new ImmutableMap.Builder<String, Double>()
            .put(V1_1, 1.1)
            .put(V1_0, 1.0)
            .build();


    private static final String ROOT_URL = "http://localhost:8080/first";

    public FirstClient() {
        super(ROOT_URL);
    }

    public VersionedEntity<List<User>> getVersionedCorporateUsers() {
        return this.versionedResource("/users", new GenericType<List<User>>() {
        });
    }

    @Override
    protected String[] generateAcceptHeader() {
        Set<String> versionsSet = VERSIONS.keySet();
        return versionsSet.toArray(new String[versionsSet.size()]);
    }

    public static Double getVersionValue(String version) {
        return VERSIONS.get(version);
    }
}
