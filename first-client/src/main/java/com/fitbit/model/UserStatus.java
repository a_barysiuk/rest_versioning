package com.fitbit.model;

/**
 * @author: Aliaksandr Barysiuk (abarysiuk@fitbit.com)
 * @date: 27/05/2014
 */
public enum UserStatus {
    ACTIVE,
    INACTIVE
}
