package com.fitbit.client;

import com.fitbit.model.VersionedEntity;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 * @author: Aliaksandr Barysiuk (abarysiuk@fitbit.com)
 * @date: 27/05/2014
 */
public abstract class GenericClient {

    protected Client client;
    protected String root;

    public GenericClient(String root) {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        this.client = Client.create(clientConfig);
        this.root = root;

    }

    public WebResource.Builder resource(String path) {
        return client.resource(root + path).accept(generateAcceptHeader());
    }

    protected <T> VersionedEntity<T> versionedResource(String path, Class<T> cl) {
        ClientResponse response = client.resource(root + path).accept(generateAcceptHeader()).get(ClientResponse.class);
        String version = response.getHeaders().getFirst("Content-type");
        return new VersionedEntity<T>(version, response.getEntity(cl));
    }

    protected <T> VersionedEntity<T> versionedResource(String path, GenericType<T> genericType) {
        ClientResponse response = client.resource(root + path).accept(generateAcceptHeader()).get(ClientResponse.class);
        String version = response.getHeaders().getFirst("Content-type");
        return new VersionedEntity<T>(version, response.getEntity(genericType));
    }

    protected abstract String[] generateAcceptHeader();

}
