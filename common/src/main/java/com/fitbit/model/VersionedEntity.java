package com.fitbit.model;

/**
 * @author: Aliaksandr Barysiuk (abarysiuk@fitbit.com)
 * @date: 27/05/2014
 */
public class VersionedEntity<T> {
    private String version;
    private T entity;

    public VersionedEntity() {
    }

    public VersionedEntity(String version, T entity) {
        this.version = version;
        this.entity = entity;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
}
