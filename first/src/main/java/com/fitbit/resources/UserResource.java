package com.fitbit.resources;

import com.fitbit.client.FirstClient;
import com.fitbit.dao.MockUserDao;
import com.fitbit.model.User;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * @author: Aliaksandr Barysiuk (abarysiuk@fitbit.com)
 * @date: 27/05/2014
 */

@Path("/users")
public class UserResource {

    private MockUserDao dao = new MockUserDao();

    @GET
    @Produces(FirstClient.V1_0)
    public List<User> listV1() {
        List<User> users = dao.finaAll();
        // users v1.0 should not contain e.g. status
        for (User user : users) {
            user.setStatus(null);
        }
        return users;
    }

    @GET
    @Produces({FirstClient.V1_1, FirstClient.V1_2})
    public List<User> listV1_1_1_2() {
        List<User> users = dao.finaAll();
        // users v1.1 and v1.2(no changes from v1.1) should not contain e.g. name
        for (User user : users) {
            user.setLastName(null);
        }
        return users;
    }

    @GET
    @Produces({FirstClient.V1_3})
    public List<User> listV1_3() {
        return dao.finaAll();
    }
}
