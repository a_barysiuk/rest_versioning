package com.fitbit.dao;

import com.fitbit.model.User;
import com.fitbit.model.UserStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author: Aliaksandr Barysiuk (abarysiuk@fitbit.com)
 * @date: 27/05/2014
 */
public class MockUserDao {

    public List<User> finaAll() {
        return Arrays.asList(new User("John","Smith",3, UserStatus.ACTIVE), new User("Adam","Sandler", 10, UserStatus.INACTIVE));
    }
}
