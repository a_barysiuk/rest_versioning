### Description
* common - contains abstract client and abstract model that support versioning
* first-client - client for "first" module. In this example may be v 1.1 and v 1.3 (support model version <=1.1 and <=1.3)
* first - server with endpoints. Contains different endpoints versions
* second-old - "weightsite" emulation. Uses first-client v 1.1 (model <=1.1)
* second-new - "weightsite" emulation. Uses first-client v 1.3 (model <=1.3)

### Running

All needed artifacts should be in local maven repo: $root/m2 - no need to build them

1. go to "first" 
2. run "gradle jettyRun"

3. go to "second-old"
4. run "gradle run"

5. go to "second-new"
6. run "gradle run"