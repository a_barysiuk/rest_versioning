package com.fitbit;

import com.fitbit.client.FirstClient;
import com.fitbit.model.User;
import com.fitbit.model.VersionedEntity;

import java.util.List;

/**
 * @author: Aliaksandr Barysiuk (abarysiuk@fitbit.com)
 * @date: 27/05/2014
 */
public class App {

    public static void main(String[] args) {
        FirstClient client = new FirstClient();

        VersionedEntity<List<User>> versionedCorporateUsers = client.getVersionedCorporateUsers();
        String version = versionedCorporateUsers.getVersion();
        Double versionValue = FirstClient.getVersionValue(version);

        System.out.println(versionValue);

        // make default logic e.g. for v 1.0

        if(versionValue > 1.0) {
            System.out.println("apply logic for 1.1+");
        }

        for (User user : versionedCorporateUsers.getEntity()) {
            System.out.println(user);
        }


    }
}
